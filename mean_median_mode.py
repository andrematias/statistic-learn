def sort(items):
    for i in range(0, len(items)):
        min_index = i
        for j in range(i+1, len(items)):
            if items[j] < items[min_index]:
                min_index = j
        items[min_index], items[i] = items[i], items[min_index]
    return items

def count(k, items):
    total = 0
    for i in items:
        if i == k:
            total += 1
    return total


def mean(n, items):
    # return sum(items) / n
    s = 0
    for i in range(0, len(items)):
        s += items[i]
    return s / n


def median(n, items):
    items = sort(items)
    middle = float(n) / 2
    mod = n % 2
    if mod != 0:
        return items[int(middle - 0.5)]
    # if len(items) > 5:
    #     return items[4]
    else:
        return (items[int(middle)] + items[int(middle) - 1]) / 2

def moda(items):
    items = sort(items)
    moda = []
    last = count(items[0], items)
    for i in items: 
        current = count(i, items)
        if current > last:
            moda.insert(0, i)
            last = current

    return moda[0] if len(moda) > 0 else items[0]


def weightMean(weight, items):
    multiply = 0
    sumWeight = 0

    if len(weight) == len(items):
        for i in range(0, len(weight)):
            sumWeight += weight[i]
            multiply += weight[i] * items[i]
    else:
        diffLen = len(items) - len(weight)
        for i in range(0, diffLen):
            weight.append(1)
        return weightMean(weight, items)

    return "{0:.1f}".format(float(multiply / sumWeight))


if __name__ == '__main__':
    n = input('Enter with a number of items in array:')
    n = int(n)

    items = list(
        map(
            lambda x: int(x), str.split(
                input('Enter with a array number separated by space:')
            )
        )
    )

    weight = input('Enter with a array number separated by space for weight:')
    weight = [int(i) for i in weight.split()]

    print("Mean: ", mean(n, items))
    print("Median: ", median(n, items))
    print("Moda: ", moda(items))
    print("Weight Mean: ", weightMean(weight, items))
