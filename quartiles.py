from mean_median_mode import median, sort

def sliceItems(items, start, stop):
    arr = list()
    for i in range(0, (stop - start) + 1):
        arr.append(items[start + i])
    return arr


def quartile(n, items):
    """
    A quartile is used to avaliable some data period by low half, middle and
    high half.
    When there is a odd number we not include a median number of items.
    That function can be implemented using a python slices objects or a raw
    function sliceItems().
    To enable a slice python implementation uncoment the follows lines 24, 27,
    30 and 31.
    """
    items = sort(items)
    middle = float(n) / 2
    mod = n % 2

    lHalfSplit = slice(0, int(middle))
    lHalfItems = sliceItems(items, 0, int(middle))

    if mod != 0 :
        # hHalfSplit = slice(int(middle) + 1, None)
        hHalfItems = sliceItems(items, int(middle) + 1, len(items) - 1)
    else:
        # hHalfSplit = slice(int(middle), None)
        hHalfItems = sliceItems(items, int(middle), len(items) - 1)

    # hHalfItems = items[hHalfSplit]
    # lHalfItems = items[lHalfSplit]


    mHalf = median(len(items), items)
    lHalf = median(len(lHalfItems), lHalfItems)
    hHalf = median(len(hHalfItems), hHalfItems)

    return lHalf, mHalf, hHalf



if __name__ == '__main__':
    n = input('Enter with a number of items in array:')
    n = int(n)

    items = list(
        map(
            lambda x: int(x), str.split(
                input('Enter with a array number separated by space:')
            )
        )
    )

    l, m, h = quartile(n, items)

    print("{0:.0f}\n{1:.0f}\n{2:.0f}".format(l, m, h))
