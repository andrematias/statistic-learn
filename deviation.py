from mean_median_mode import mean

def standartDeviation(n, items):
    m = mean(n, items)
    total = 0
    for i in items:
        total += (i - m)**2
    return (total/n)**(1/2)


if __name__ == '__main__':
    n = int(input('Enter with a number of items in array:'))
    items = [int(i) for i in input("Enter with a array numbers separated by space:").strip().split()]
    print("{0:.1f}".format(standartDeviation(n, items)))
